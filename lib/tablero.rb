require "./lib/punto"
require "./lib/jugador"

class Tablero

    def initialize
        @fila=4
        @columna=4
        @jugador1 = Jugador.new(" ")
        @jugador2 = Jugador.new(" ")
        @matriz=Array.new(@fila) {Array.new(@columna) {Punto.new()}}
    end

    def inicializarTablero
        for i in 0..3 do
            for j in 0..3 do
                @matriz[i][j].setArriba("abierto")
                @matriz[i][j].setAbajo("abierto")
                @matriz[i][j].setIzq("abierto")
                @matriz[i][j].setDer("abierto")
            end
        end
        for i in 0..3 do
            @matriz[0][i].setArriba("limite")
            @matriz[3][i].setAbajo("limite")
            @matriz[i][0].setIzq("limite")
            @matriz[i][3].setDer("limite")
        end
    end

    def guardarJugador(jugador1,jugador2)
        @jugador1.setNombre(jugador1)
        @jugador2.setNombre(jugador2)
    end

    def tipoJugada(x1,y1,x2,y2)
            if x1 == x2
                return "horizontal"
            else
                if y1==y2
                    return "vertical"
                else
                    return "error"
                end
            end  
    end

    #p1(2.2) p2(2.1)//  p1(2.2) p2(3.2)
    def devolverMayor(x1,y1,x2,y2)
        if x1==x2
            if y1>y2 && (y1-y2==1)
                return "p1"
            else
                if y2>y1 &&(y2-y1==1)
                    return "p2"
                else
                    return "puntos separados por mas de 2 espacios"
                end
                
            end
        else
            if y1==y2
                if x1>x2 && (x1-x2==1)
                    return "p1"
                else
                    if x2>x1 && (x2-x1==1)
                        return "p2"
                    else
                        return "puntos separados por mas de 2 espacios"
                    end
                    
                end
            end
        end
    end

    def verificarCoordenadas(x1,y1,x2,y2)
        if (x1 < @fila && x1 >= 0 && y1 < @columna && y1 >= 0)
            if (x2 < @fila && x2 >= 0 && y2 < @columna && y2 >= 0)
                return true
            end
        end
        return false
    end

    def esJugadasIguales(x1,y1,x2,y2)
        if x1==x2 && y1==y2
            return false
        end
        return true
    end

    def registrarJugada(x1,y1,x2,y2,jugador)
        if (verificarCoordenadas(x1,y1,x2,y2) && esJugadasIguales(x1,y1,x2,y2) )
            if tipoJugada(x1,y1,x2,y2)=="horizontal"
                if devolverMayor(x1,y1,x2,y2)=="p2"
                    @matriz[x1][y1].setDer("cerrado")
                    @matriz[x2][y2].setIzq("cerrado")
                    esCuadradoAbajo(x1,y1,x2,y2,jugador)
                    esCuadradoArriba(x1,y1,x2,y2,jugador)
                else
                    @matriz[x1][y1].setIzq("cerrado")
                    @matriz[x2][y2].setDer("cerrado")
                    #se cambio la pos de los coordenadas x,y si P2(x,y) es menor
                    esCuadradoAbajo(x2,y2,x1,y1,jugador)
                    esCuadradoArriba(x2,y2,x1,y1,jugador)
                end
            else
                if tipoJugada(x1,y1,x2,y2)=="vertical"
                    if devolverMayor(x1,y1,x2,y2)=="p2"
                        @matriz[x1][y1].setAbajo("cerrado")
                        @matriz[x2][y2].setArriba("cerrado")
                        esCuadradoIzq(x1,y1,x2,y2,jugador)
                        esCuadradoDer(x1,y1,x2,y2,jugador)
                    else
                        @matriz[x1][y1].setArriba("cerrado")
                        @matriz[x2][y2].setAbajo("cerrado")
                        esCuadradoIzq(x2,y2,x1,y1,jugador)
                        esCuadradoDer(x2,y2,x1,y1,jugador)
                    end
                end
            end
        end
        
    end

    def getPunto(x,y)
        @matriz[x][y]
    end

    def getTablero
        @matriz
    end

    def getJugador1
        @jugador1
    end

    def getJugador2
        @jugador2
    end

    def esCuadradoAbajo(x1,y1,x2,y2,jugador)
        if @matriz[x1][y1].getAbajo != "limite"
            if (@matriz[x1+1][y1].getAbajo == "cerrado" && 
                @matriz[x1+1][y1].getDer == "cerrado" &&
                @matriz[x2+1][y2].getAbajo == "cerrado") 
                if jugador == "jugador1"
                    @jugador1.aumentarPuntaje
                else
                    @jugador2.aumentarPuntaje 
                end
            end
        end
    end

    def esCuadradoArriba(x1,y1,x2,y2,jugador)
        if @matriz[x1][y1].getArriba != "limite"
            c1=x1-1
            c2=x2-1
            if (@matriz[x1-1][y1].getAbajo == "cerrado" && 
                @matriz[x1-1][y1].getDer == "cerrado" &&
                @matriz[x2-1][y2].getAbajo == "cerrado")
                if jugador == "jugador1"
                    @jugador1.aumentarPuntaje
                else
                    @jugador2.aumentarPuntaje 
                end
            end
        end
    end

    def esCuadradoIzq(x1,y1,x2,y2,jugador)
            if @matriz[x1][y1].getIzq != "limite"
                if (@matriz[x1][y1-1].getDer == "cerrado" && 
                    @matriz[x1][y1-1].getAbajo == "cerrado" &&
                    @matriz[x2][y2-1].getDer == "cerrado")
                    if jugador == "jugador1"
                        @jugador1.aumentarPuntaje
                    else
                        @jugador2.aumentarPuntaje 
                    end
                end
            end
    end

    def esCuadradoDer(x1,y1,x2,y2,jugador)
        if @matriz[x1][y1].getDer != "limite"
            if (@matriz[x1][y1+1].getIzq=="cerrado" && 
                @matriz[x1][y1+1].getAbajo=="cerrado" &&
                @matriz[x2][y2+1].getIzq=="cerrado") 
                if jugador == "jugador1"
                    @jugador1.aumentarPuntaje
                else
                    @jugador2.aumentarPuntaje 
                end
            end
        end
    end

    

    

    

end