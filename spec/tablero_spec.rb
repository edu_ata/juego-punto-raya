require "./lib/tablero"
require "./lib/punto"
require "./lib/jugador"

describe Tablero do
    it "Al crear un nuevo tablero deberia tener mis atributos por defecto" do
        t=Tablero.new()
        j1=Jugador.new("")
        j2=Jugador.new("")
        p=Punto.new()
        j1=t.getJugador1
        j2=t.getJugador2
        p=t.getPunto(0,0)
        expect(j1.getNombre).to eq " "
        expect(j2.getNombre).to eq " "
        expect(p.getArriba).to eq "abierto"
        expect(p.getAbajo).to eq "abierto"
        expect(p.getDer).to eq "abierto"
        expect(p.getIzq).to eq "abierto"
    end

    it "al inicializar una nueva partida el tablero debe estar disponible y asignar los limites a los puntos" do
        t=Tablero.new()
        p1=Punto.new()
        p2=Punto.new()
        t.inicializarTablero
        p1=t.getPunto(0,0)
        p2=t.getPunto(1,2)
        expect(p1.getArriba).to eq "limite"
        expect(p1.getAbajo).to eq "abierto"
        expect(p1.getDer).to eq "abierto"
        expect(p1.getIzq).to eq "limite"
        
        expect(p2.getArriba).to eq "abierto"
        expect(p2.getAbajo).to eq "abierto"
        expect(p2.getDer).to eq "abierto"
        expect(p2.getIzq).to eq "abierto"
    end

    it "deberia poder guardar los nombres de los jugadores" do
        t=Tablero.new()
        j1=Jugador.new("")
        j2=Jugador.new("")
        t.guardarJugador("Edwin","Robert")
        j1=t.getJugador1
        j2=t.getJugador2
        expect(j1.getNombre).to eq "Edwin"
        expect(j2.getNombre).to eq "Robert"
    end

    it "si tengo los 2 puntos en la misma fila seria jugada horizontal " do
        t=Tablero.new()
        expect(t.tipoJugada(0,0,0,1)).to eq "horizontal"
    end

    it "si tengo los 2 puntos en la misma columna seria jugada vertical " do
        t=Tablero.new()
        expect(t.tipoJugada(0,0,1,0)).to eq "vertical"
    end

    it "si tengo los 2 puntos en diferentes fila y columnas estaria mal " do
        t=Tablero.new()
        expect(t.tipoJugada(0,0,1,1)).to eq "error"
    end

    it "si tengo las coordenadas de 2 puntos que exites deberia devolver true" do
        t=Tablero.new()
        expect(t.verificarCoordenadas(0,0,1,1)).to eq true    
    end

    it "si tengo las coordenadas de 2 puntos que NO exites deberia devolver false" do
        t=Tablero.new()
        expect(t.verificarCoordenadas(0,0,4,-1)).to eq false    
    end

    it "si tengo 2 puntos deberia devolver el punto mayor" do
        t=Tablero.new()
        #p1(0,1)
        #p2(1,1)
        expect(t.devolverMayor(0,1,1,1)).to eq "p2"
        #p1(1,2)
        #p2(1,1)
        expect(t.devolverMayor(1,2,1,1)).to eq "p1"    
    end

    it "si tengo las coordenadas de 2 puntos que son los mismos no deberia marcar jugada" do
        t=Tablero.new()
        #p1(1,2)
        #p2(1,2)
        expect(t.esJugadasIguales(1,2,1,2)).to eq false    
    end

    it "si tengo 2 puntos que estan distanciados devolveria error" do
        t=Tablero.new()
        #p1(0,1)
        #p2(3,1)
        expect(t.devolverMayor(0,1,3,1)).to eq "puntos separados por mas de 2 espacios"
    end

    it "Al mandar las coordenadas de 2 puntos en 'Horizontal' deberia marcarme esas lineas como 'cerrado'" do
        t=Tablero.new()
        
        p1=Punto.new()
        p2=Punto.new()
        t.inicializarTablero
        #p1(0,0)
        #p2(0,1)
        t.registrarJugada(0,0,0,1,"jugador1")
        p1=t.getPunto(0,0)
        p2=t.getPunto(0,1)
        expect(p1.getDer).to eq "cerrado"
        expect(p2.getIzq).to eq "cerrado"
        
    end

    it "Al mandar las coordenadas de 2 puntos en 'Vertical' deberia marcarme esas lineas como 'cerrado'" do
        t=Tablero.new()
        
        p1=Punto.new()
        p2=Punto.new()
        t.inicializarTablero
        #p1(1,1)
        #p2(2,1)
        t.registrarJugada(1,1,2,1,"jugador1")
        p1=t.getPunto(1,1)
        p2=t.getPunto(2,1)
        expect(p1.getAbajo).to eq "cerrado"
        expect(p2.getArriba).to eq "cerrado"
    end

    it "Si completo un cuadrado deberia aumentar mi puntaje  en 1" do
        t=Tablero.new()
        j1=Jugador.new("")
        j2=Jugador.new("")
        t.inicializarTablero
        #p1(0,0)
        #p2(0,1)
        t.registrarJugada(0,0,0,1,"jugador1")
        #p1(0,0)
        #p2(1,0)
        t.registrarJugada(0,0,1,0,"jugador2")
        #p1(0,1)
        #p2(1,1)
        t.registrarJugada(0,1,1,1,"jugador1")
        #p1(1,0)
        #p2(1,1)
        t.registrarJugada(1,0,1,1,"jugador2")
        j2=t.getJugador2
        expect(j2.getPuntaje).to eq 1
       
    end


    

end